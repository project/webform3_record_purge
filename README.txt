Webform3 Record Purge

This is an add-on for Webform 3 to allow automated purging of records of webforms after a specified timeframe.
Options are configurable per webform. This module is for people who for whatever reason are not in a position to upgrade their webforms to Webform 4, but need to automate removal of submission data.

Purging happens on cron runs. Cron must be running for this module to function.

NB As of writing, the current version of Webform 4 has this functionality built in.
